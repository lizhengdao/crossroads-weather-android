package com.umbrellait.weather.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

data class WeatherEntity(
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "main") val main: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "icon") val icon: String
)