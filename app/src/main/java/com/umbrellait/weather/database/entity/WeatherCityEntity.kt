package com.umbrellait.weather.database.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "weather_city")
data class WeatherCityEntity(
    @Embedded(prefix = "coord_") val coord: CoordEntity,
    @Embedded(prefix = "weather_") val weather: WeatherEntity,
    @Embedded(prefix = "main_") val main: MainEntity,
    @Embedded(prefix = "wind_") val wind: WindEntity,
    @Embedded(prefix = "clouds_") val clouds: CloudsEntity,
    @Embedded(prefix = "rain_") val rain: RainEntity,
    @Embedded(prefix = "snow_") val snow: SnowEntity,
    @ColumnInfo(name = "dt") val dt: Long,
    @Embedded(prefix = "son_") val son: SonEntity,
    @ColumnInfo(name = "timezone") val timezone: Int,
    @ColumnInfo(name = "id") @PrimaryKey val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "visibility") val visibility: Int
)

data class CoordEntity(
    @ColumnInfo(name = "lat") val lat: String,
    @ColumnInfo(name = "lon") val lon: String
)

data class MainEntity(
    @ColumnInfo(name = "feels_like") val feelsLike: Double,
    @ColumnInfo(name = "humidity") val humidity: Int,
    @ColumnInfo(name = "pressure") val pressure: Int,
    @ColumnInfo(name = "temp") val temp: Int
)

data class WindEntity(
    @ColumnInfo(name = "deg") val deg: Int,
    @ColumnInfo(name = "speed") val speed: Float
)

data class CloudsEntity(
    @ColumnInfo(name = "all") val all: Int
)

data class RainEntity(
    @ColumnInfo(name = "last_hour") val lastHour: Double
)

data class SnowEntity(
    @ColumnInfo(name = "last_hour") val lastHour: Double
)

data class SonEntity(
    @ColumnInfo(name = "sunrise") val sunrise: Long,
    @ColumnInfo(name = "sunset") val sunset: Long
)