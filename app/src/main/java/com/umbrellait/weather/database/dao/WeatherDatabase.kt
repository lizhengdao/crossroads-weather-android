package com.umbrellait.weather.database.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.umbrellait.weather.database.entity.DailyEntity
import com.umbrellait.weather.database.entity.WeatherCityEntity

@Database(entities = [WeatherCityEntity::class, DailyEntity::class], version = 1, exportSchema = false)
abstract class WeatherDatabase: RoomDatabase() {
    abstract val weatherDao: WeatherDao
}