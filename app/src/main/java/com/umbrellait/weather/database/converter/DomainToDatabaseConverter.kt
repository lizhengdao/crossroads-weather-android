package com.umbrellait.weather.database.converter

import com.umbrellait.weather.database.entity.*
import com.umbrellait.weather.domain.*

class DomainToDatabaseConverter {

    fun toWeatherCityEntity(weatherCity: WeatherCity): WeatherCityEntity {
        return WeatherCityEntity(
            coord = toCoord(weatherCity.coord),
            weather = toWeather(weatherCity.weather),
            main = toMain(weatherCity.main),
            wind = toWind(weatherCity.wind),
            clouds = toClouds(weatherCity.clouds),
            rain = toRain(weatherCity.rain),
            snow = toSnow(weatherCity.snow),
            dt = weatherCity.dt,
            son = toSon(weatherCity.son),
            timezone = weatherCity.timezone,
            id = weatherCity.id,
            name = weatherCity.name,
            visibility = weatherCity.visibility
        )
    }

    private fun toCoord(coord: Coord): CoordEntity {
        return CoordEntity(
            lat = coord.lat,
            lon = coord.lon
        )
    }

    private fun toWeather(weather: Weather): WeatherEntity {
        return WeatherEntity(
            id = weather.id,
            main = weather.main,
            description = weather.description,
            icon = weather.icon
        )
    }

    private fun toMain(main: Main): MainEntity {
        return MainEntity(
            feelsLike = main.feelsLike,
            humidity = main.humidity,
            pressure = main.pressure,
            temp = main.temp
        )
    }

    private fun toWind(wind: Wind): WindEntity {
        return WindEntity(
            deg = wind.deg,
            speed = wind.speed
        )
    }

    private fun toClouds(clouds: Clouds): CloudsEntity {
        return CloudsEntity(
            all = clouds.all
        )
    }

    private fun toRain(rain: Rain): RainEntity {
        return RainEntity(
            lastHour = rain.lastHour
        )
    }

    private fun toSnow(snow: Snow): SnowEntity {
        return SnowEntity(
            lastHour = snow.lastHour
        )
    }

    private fun toSon(son: Son): SonEntity {
        return SonEntity(
            sunset = son.sunset,
            sunrise = son.sunrise
        )
    }

    fun toDailyEntityList(dailyList: List<Daily>): List<DailyEntity> {
        return dailyList.map { toDailyEntity(it) }
    }

    private fun toDailyEntity(daily: Daily): DailyEntity {
        return DailyEntity(
            lat = daily.lat,
            lon = daily.lon,
            clouds = daily.clouds,
            dt = daily.dt,
            feelsLike = toFeelsLike(daily.feelsLike),
            humidity = daily.humidity,
            pressure = daily.pressure,
            sunrise = daily.sunrise,
            sunset = daily.sunset,
            temp = toTemp(daily.temp),
            uvi = daily.uvi,
            weather = toWeather(daily.weather),
            windDeg = daily.windDeg,
            windSpeed = daily.windSpeed
        )
    }

    private fun toFeelsLike(feelsLike: FeelsLike): FeelsLikeEntity {
        return FeelsLikeEntity(
            morn = feelsLike.morn,
            day = feelsLike.day,
            eve = feelsLike.eve,
            night = feelsLike.night
        )
    }

    private fun toTemp(temp: Temp): TempEntity {
        return TempEntity(
            morn = temp.morn,
            day = temp.day,
            eve = temp.eve,
            night = temp.night
        )
    }

}
