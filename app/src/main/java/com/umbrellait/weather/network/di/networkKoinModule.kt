package com.umbrellait.weather.network.di

import com.umbrellait.weather.BuildConfig
import com.umbrellait.weather.network.Network
import com.umbrellait.weather.network.NetworkImpl
import com.umbrellait.weather.network.interceptor.ApiKeyInterceptor
import com.umbrellait.weather.network.api.ApiWeather
import com.umbrellait.weather.network.baseUri
import com.umbrellait.weather.network.converter.NetworkToDomainConverter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkKoinModule: Module = module {

    single { provideHttpLoggingInterceptor() }
    single { provideApiKeyInterceptor() }

    single {
        provideHttpClient(
            httpLoggingInterceptor = get(),
            apiKeyInterceptor = get()
        )
    }

    single { provideGsonConverterFactory() }

    single {
        provideWeatherRetrofit(
            httpClient = get(),
            converterFactory = get()
        )
    }

    single { provideApiWeather(retrofit = get()) } bind ApiWeather::class

    single { providerNetworkToDomainConverter() }

    single {
        NetworkImpl(
            apiWeather = get(),
            networkToDomainConverter = get()
        )
    } bind (Network::class)
}

fun provideHttpLoggingInterceptor() = HttpLoggingInterceptor().apply {
    level = if (BuildConfig.DEBUG) {
        HttpLoggingInterceptor.Level.BODY
    } else {
        HttpLoggingInterceptor.Level.NONE
    }
}

fun provideApiKeyInterceptor() = ApiKeyInterceptor()

fun provideHttpClient(
    httpLoggingInterceptor: HttpLoggingInterceptor,
    apiKeyInterceptor: ApiKeyInterceptor
): OkHttpClient {
    return OkHttpClient()
        .newBuilder()
        .addNetworkInterceptor(httpLoggingInterceptor)
        .addInterceptor(apiKeyInterceptor)
        .connectTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .build()
}

fun provideGsonConverterFactory(): Converter.Factory = GsonConverterFactory.create()

fun provideWeatherRetrofit(
    httpClient: OkHttpClient,
    converterFactory: Converter.Factory
): Retrofit {

    return Retrofit.Builder()
        .client(httpClient)
        .baseUrl(baseUri)
        .addConverterFactory(converterFactory)
        .build()
}

fun provideApiWeather(retrofit: Retrofit): ApiWeather {
    return retrofit.create(ApiWeather::class.java)
}

fun providerNetworkToDomainConverter() = NetworkToDomainConverter()