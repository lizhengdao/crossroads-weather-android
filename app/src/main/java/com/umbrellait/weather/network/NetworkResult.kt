package com.umbrellait.weather.network

sealed class NetworkResult<out T: Any> {
    data class Success<out T : Any>(val data: T) : NetworkResult<T>()
    data class Error(val message: String, val exception: Exception? = null, val code: Int? = null) : NetworkResult<Nothing>()
}