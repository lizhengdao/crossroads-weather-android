package com.umbrellait.weather.network

import com.umbrellait.weather.domain.Daily
import com.umbrellait.weather.domain.WeatherCity

interface Network {
    suspend fun getWeatherCity(nameCity: String): NetworkResult<WeatherCity>

    suspend fun getWeatherCityList(idList: String): NetworkResult<List<WeatherCity>>

    suspend fun getWeatherCityInSevenDays(lat: String, lon: String): NetworkResult<List<Daily>>
}