package com.umbrellait.weather.network.interceptor

enum class AuthorizedType {
    API_KEY,
    NONE
}