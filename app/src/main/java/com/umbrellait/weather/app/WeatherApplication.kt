package com.umbrellait.weather.app

import android.app.Application
import com.umbrellait.weather.database.di.databaseKoinModule
import com.umbrellait.weather.network.di.networkKoinModule
import com.umbrellait.weather.repository.di.repositoryKoinModule
import com.umbrellait.weather.ui.uiKoinModule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class WeatherApplication : Application() {

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@WeatherApplication)
            modules(
                listOf(
                    networkKoinModule,
                    databaseKoinModule,
                    repositoryKoinModule,
                    uiKoinModule
                )
            )
        }
    }
}