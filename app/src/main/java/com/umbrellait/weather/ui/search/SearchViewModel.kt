package com.umbrellait.weather.ui.search

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.umbrellait.weather.domain.Result
import com.umbrellait.weather.domain.WeatherCity
import com.umbrellait.weather.repository.Repository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@FlowPreview
@ExperimentalCoroutinesApi
class SearchViewModel(
    private val repository: Repository
) : ViewModel() {

    private val eventsChannel = BroadcastChannel<SearchViewEvent>(capacity = Channel.CONFLATED)

    private val _state = MutableStateFlow(SearchViewState())
    val state: StateFlow<SearchViewState>
        get() = _state

    private val eventsFlow = eventsChannel.asFlow()

    private val searchDataFlow = eventsFlow.filterIsInstance<SearchViewEvent.SearchDataEvent>()
        .debounce(1000)
        .filter { it.text.isNotEmpty() }
        .flatMapLatest {
            flowOf(repository.getWeatherCity(it.text, false))
                .onStart { emit(Result.Loading) }
        }
        .map { SearchView.toSearchView(it) }
        .map { SearchView.toPartialViewState(it) }

    private val addCityDataFlow = eventsFlow.filterIsInstance<SearchViewEvent.AddCityDataEvent>()
        .debounce(1000)
        .filter { it.text.isNotEmpty() }
        .flatMapLatest {
            flowOf(repository.getWeatherCity(it.text, true))
                .onStart { emit(Result.Loading) }
        }
        .map { AddCityView.toAddCityView(it) }
        .map { AddCityView.toPartialViewState(it) }

    init {
        merge(searchDataFlow, addCityDataFlow)
            .scan(SearchViewState(), { accumulator, value -> value.invoke(accumulator) })
            .onEach {
                _state.value = it
            }
            .launchIn(viewModelScope)
    }

    suspend fun sendSearchViewEvent(searchViewEvent: SearchViewEvent) {
        eventsChannel.send(searchViewEvent)
    }
}