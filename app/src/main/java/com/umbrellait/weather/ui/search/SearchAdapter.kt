package com.umbrellait.weather.ui.search

import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.umbrellait.weather.domain.items.DisplayableItem
import com.umbrellait.weather.ui.delegate.buttonAdapterDelegate
import com.umbrellait.weather.ui.delegate.emptyAdapterDelegate
import com.umbrellait.weather.ui.delegate.cardAdapterDelegate

class SearchAdapter(
    addButtonClickedListener: () -> Unit
) : ListDelegationAdapter<List<DisplayableItem>>(
    cardAdapterDelegate(),
    buttonAdapterDelegate(addButtonClickedListener),
    emptyAdapterDelegate()
)