package com.umbrellait.weather.ui.several_days

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.umbrellait.weather.R
import com.umbrellait.weather.databinding.SeveralDaysFragmentBinding
import com.umbrellait.weather.domain.items.CardState
import com.umbrellait.weather.domain.items.ErrorState
import com.umbrellait.weather.ui.LinearItemDividers
import com.umbrellait.weather.utils.CardSource
import com.umbrellait.weather.utils.dpToPx
import com.umbrellait.weather.utils.setThrottledClickListener
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

@FlowPreview
@ExperimentalCoroutinesApi
class SeveralDaysFragment : Fragment() {

    private var _binding: SeveralDaysFragmentBinding? = null
    private val binding get() = _binding!!

    private val severalDaysViewModel: SeveralDaysViewModel by viewModel {
        parametersOf(
            arguments?.let { SeveralDaysFragmentArgs.fromBundle(it).lat },
            arguments?.let { SeveralDaysFragmentArgs.fromBundle(it).lon }
        )
    }

    private val severalDaysAdapter: SeveralDaysAdapter = SeveralDaysAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SeveralDaysFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        severalDaysViewModel.state
            .onEach {
                handlerState(it)
            }
            .launchIn(lifecycleScope)

        binding.severalRecyclerView.adapter = severalDaysAdapter
        binding.severalRecyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.severalRecyclerView.addItemDecoration(
            LinearItemDividers(
                0.dpToPx,
                12.dpToPx,
                ContextCompat.getColor(requireContext(), R.color.colorWhite)
            )
        )
        PagerSnapHelper().attachToRecyclerView(binding.severalRecyclerView)

        binding.backArrow.setThrottledClickListener { findNavController().navigateUp() }

        binding.titleTextView.text = arguments?.let { SeveralDaysFragmentArgs.fromBundle(it).nameCity } ?: ""

        binding.swipeRefreshLayout.setOnRefreshListener {
            lifecycleScope.launchWhenStarted { severalDaysViewModel.sendSeveralDaysViewEvent(SeveralDaysViewEvent.LoadDataEvent) }
        }

        lifecycleScope.launchWhenStarted { severalDaysViewModel.sendSeveralDaysViewEvent(SeveralDaysViewEvent.LoadDataEvent) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun handlerState(severalDaysViewState: SeveralDaysViewState) {
        when(severalDaysViewState.severalDaysView) {
            is SeveralDaysView.Loading -> {
                binding.swipeRefreshLayout.isRefreshing = true
            }
            is SeveralDaysView.Error -> {
                binding.swipeRefreshLayout.isRefreshing = false
                severalDaysAdapter.items = listOf(ErrorState)
                severalDaysAdapter.notifyDataSetChanged()
            }
            is SeveralDaysView.Success -> {
                binding.swipeRefreshLayout.isRefreshing = false

                val cardStateList: ArrayList<CardState> = arrayListOf()
                severalDaysViewState.severalDaysView.dailyList.forEach {
                    cardStateList.add(CardState(weatherCity = null, daily = it, cardSource = CardSource.SEVERAL))
                }
                severalDaysAdapter.items = cardStateList
                severalDaysAdapter.notifyDataSetChanged()
            }
        }
    }

}