package com.umbrellait.weather.ui.delegate

import android.view.View
import androidx.core.content.ContextCompat
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import com.umbrellait.weather.R
import com.umbrellait.weather.databinding.*
import com.umbrellait.weather.domain.WeatherCity
import com.umbrellait.weather.domain.items.*
import com.umbrellait.weather.utils.CardSource
import com.umbrellait.weather.utils.setThrottledClickListener
import com.umbrellait.weather.utils.toHoursAndMinutesTime
import com.umbrellait.weather.utils.toWeatherIcon
import com.umbrellait.weather.utils.toWindDirectionString
import java.util.*

fun weatherCityLineAdapterDelegate(itemClickedListener: (WeatherCity) -> Unit) =
    adapterDelegateViewBinding<WeatherCity, DisplayableItem, ItemListOfCitiesBinding>(

        viewBinding = { layoutInflater, root ->
            ItemListOfCitiesBinding.inflate(
                layoutInflater,
                root,
                false
            )
        },

        on = { item: Any, _: List<Any>, _: Int -> item is WeatherCity }
    ) {
        binding.root.setThrottledClickListener {
            itemClickedListener(item)
        }

        bind {
            binding.cityTextView.text = item.name
            binding.temperatureTexView.text = "${item.main.temp}"
            binding.iconImageView.setImageResource(item.weather.icon.toWeatherIcon())
        }
    }

fun cardAdapterDelegate() = adapterDelegateViewBinding<CardState, DisplayableItem, ItemCardBinding>(
    viewBinding = {  layoutInflater, root ->
        ItemCardBinding.inflate(
            layoutInflater,
            root,
            false)
    },

    on = { item, _, _ ->  item is CardState }
) {

    bind {

        with(binding) {
            root.clipToOutline = true

            when(item.cardSource) {
                CardSource.SEARCH -> {
                    dateTextView.text = android.text.format.DateFormat.format("d MMMM", (item.weatherCity?.dt ?: 0) * 1000).toString()
                    degreeTextView.text = item.weatherCity?.main?.temp?.toString()
                    weatherTextView.text = item.weatherCity?.weather?.description?.capitalize()
                    weatherImageView.setImageResource(item.weatherCity?.weather?.icon?.toWeatherIcon() ?: R.drawable.ic_clear_sky_day)

                    windView.setText("${item.weatherCity?.wind?.deg?.toWindDirectionString()} ${item.weatherCity?.wind?.speed} м/с")
                    temperatureView.setText("${item.weatherCity?.main?.feelsLike}°")
                    humidityView.setText("${item.weatherCity?.main?.humidity}%")
                    pressureView.setText("${item.weatherCity?.main?.pressure}")

                }
                CardSource.DETAIL -> {
                    val calendar = GregorianCalendar.getInstance()
                    calendar.timeInMillis = (item.weatherCity?.dt ?: 0) * 1000
                    dateTextView.text = android.text.format.DateFormat.format("d MMMM", calendar.time.time).toString()
                    degreeTextView.text = item.weatherCity?.main?.temp?.toString()
                    weatherTextView.text = item.weatherCity?.weather?.description?.capitalize()
                    weatherImageView.setImageResource(item.weatherCity?.weather?.icon?.toWeatherIcon() ?: R.drawable.ic_clear_sky_day)

                    windView.setText("${item.weatherCity?.wind?.deg?.toWindDirectionString()} ${item.weatherCity?.wind?.speed} м/с")
                    temperatureView.setText("${item.weatherCity?.main?.feelsLike}°")
                    humidityView.setText("${item.weatherCity?.main?.humidity}%")
                    pressureView.setText("${item.weatherCity?.main?.pressure}")

                    sunriseView.setText(((item.weatherCity?.son?.sunrise ?: 0) * 1000).toHoursAndMinutesTime())
                    sunsetView.setText(((item.weatherCity?.son?.sunset ?: 0) * 1000).toHoursAndMinutesTime())

                }
                CardSource.SEVERAL -> {
                    dateTextView.text = android.text.format.DateFormat.format("d MMMM", (item.daily?.dt ?: 0) * 1000).toString()
                    dayView.setText(item.daily?.temp?.day.toString())
                    item.daily?.weather?.icon?.toWeatherIcon()?.let { it -> dayView.setIcon(it) }
                    nightView.setText(item.daily?.temp?.night.toString())
                    item.daily?.weather?.icon?.toWeatherIcon()?.let { it -> nightView.setIcon(it) }
                    morningView.setText(item.daily?.temp?.morn.toString())
                    item.daily?.weather?.icon?.toWeatherIcon()?.let { it -> morningView.setIcon(it) }
                    eveningView.setText(item.daily?.temp?.eve.toString())
                    item.daily?.weather?.icon?.toWeatherIcon()?.let { it -> eveningView.setIcon(it) }
                    weatherTextView.text = item.daily?.weather?.description?.capitalize()
                    weatherImageView.setImageResource(item.daily?.weather?.icon?.toWeatherIcon() ?: R.drawable.ic_clear_sky_day)

                    windView.setText("${item.daily?.windDeg?.toWindDirectionString()} ${item.daily?.windSpeed} м/с")
                    //интресный момент. Вьюха одна, а значения 4
                    temperatureView.setText("${item.daily?.feelsLike?.day}°")
                    humidityView.setText("${item.daily?.humidity}%")
                    pressureView.setText("${item.daily?.pressure}")

                    sunriseView.setText(((item.daily?.sunrise ?: 0) * 1000).toHoursAndMinutesTime())
                    sunsetView.setText(((item.daily?.sunset ?: 0) * 1000).toHoursAndMinutesTime())
                }
            }
        }

        when(item.cardSource) {
            CardSource.SEARCH -> {
                binding.degreeLayout.visibility = View.VISIBLE
                binding.dayLayout.visibility = View.GONE
                binding.sunsetView.visibility = View.GONE
                binding.sunriseView.visibility = View.GONE
            }
            CardSource.DETAIL -> {
                binding.degreeLayout.visibility = View.VISIBLE
                binding.dayLayout.visibility = View.GONE
                binding.sunsetView.visibility = View.VISIBLE
                binding.sunriseView.visibility = View.VISIBLE
            }
            CardSource.SEVERAL -> {
                binding.degreeLayout.visibility = View.GONE
                binding.dayLayout.visibility = View.VISIBLE
            }
        }
    }
}

fun emptyAdapterDelegate() = adapterDelegateViewBinding<EmptyState, DisplayableItem, ItemEmptyBinding>(

    viewBinding = { layoutInflater, root ->
        ItemEmptyBinding.inflate(
            layoutInflater,
            root,
            false
        )
    },

    on = { item, _, _ -> item is EmptyState }
) {
    bind {

    }
}

fun errorAdapterDelegate() = adapterDelegateViewBinding<ErrorState, DisplayableItem, ItemErrorBinding>(

    viewBinding = { layoutInflater, root ->
        ItemErrorBinding.inflate(
            layoutInflater,
            root,
            false
        )
    },

    on = { item, _, _ -> item is ErrorState }
) {
    bind {

    }
}

fun buttonAdapterDelegate(itemClickedListener: () -> Unit = {}) = adapterDelegateViewBinding<ButtonState, DisplayableItem, ItemButtonBinding>(

    viewBinding = { layoutInflater, root ->
        ItemButtonBinding.inflate(
            layoutInflater,
            root,
            false
        )
    },

    on = { item, _, _ -> item is ButtonState }
) {

    bind {
        binding.addButton.setThrottledClickListener {
            itemClickedListener()
        }
        binding.addButton.text = item.text
        binding.addButton.setCompoundDrawables(
            ContextCompat.getDrawable(context, item.icon ?: R.drawable.ic_add), null, null, null)
    }
}
