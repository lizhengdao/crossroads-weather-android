package com.umbrellait.weather.ui.several_days

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.umbrellait.weather.domain.Result
import com.umbrellait.weather.repository.Repository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*


@FlowPreview
@ExperimentalCoroutinesApi
class SeveralDaysViewModel(
    private val repository: Repository,
    private val lat: String,
    private val lon: String
) : ViewModel() {

    private val eventsChannel = BroadcastChannel<SeveralDaysViewEvent>(capacity = Channel.CONFLATED)

    private val _state = MutableStateFlow(SeveralDaysViewState())
    val state: StateFlow<SeveralDaysViewState>
        get() = _state

    private val eventsFlow = eventsChannel.asFlow()

    private val loadDataFlow = eventsFlow.filterIsInstance<SeveralDaysViewEvent.LoadDataEvent>()
        .flatMapLatest {
            flowOf(repository.getDailyList(lat = lat, lon = lon))
                .onStart { emit(Result.Loading) }
        }
        .map { SeveralDaysView.toSeveralDaysView(it) }
        .map { SeveralDaysView.toPartialViewState(it) }

    init {
        merge(loadDataFlow)
            .scan(SeveralDaysViewState(), { accumulator, value -> value.invoke(accumulator) })
            .onEach { _state.value = it }
            .launchIn(viewModelScope)
    }

    suspend fun sendSeveralDaysViewEvent(severalDaysViewEvent: SeveralDaysViewEvent) {
        eventsChannel.send(severalDaysViewEvent)
    }
}