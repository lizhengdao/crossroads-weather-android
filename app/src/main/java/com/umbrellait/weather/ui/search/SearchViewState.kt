package com.umbrellait.weather.ui.search

import com.umbrellait.weather.R
import com.umbrellait.weather.domain.Result
import com.umbrellait.weather.domain.WeatherCity
import com.umbrellait.weather.domain.items.ButtonState
import com.umbrellait.weather.domain.items.CardState
import com.umbrellait.weather.domain.items.DisplayableItem
import com.umbrellait.weather.domain.items.EmptyState
import com.umbrellait.weather.utils.CardSource

private typealias PartialViewState = (SearchViewState) -> SearchViewState

data class SearchViewState(
    val searchView: SearchView = SearchView.Empty,
    val addCityView: AddCityView = AddCityView.Empty
)

sealed class SearchView {
    data class Success(val list: List<DisplayableItem>) : SearchView()
    data class Error(val list: List<DisplayableItem>) : SearchView()
    object Empty : SearchView()
    object Loading : SearchView()

    companion object {
        fun toSearchView(result: Result<WeatherCity>): SearchView =
            when (result) {
                is Result.Loading -> Loading
                is Result.Error -> Error(listOf(EmptyState))
                is Result.Success -> {
                    Success(
                        listOf(
                            CardState(
                                weatherCity = result.data,
                                cardSource = CardSource.SEARCH,
                                daily = null
                            ),
                            ButtonState(
                                text = "Добавить",
                                icon = R.drawable.ic_add
                            )
                        )
                    )
                }
            }

        fun toPartialViewState(searchView: SearchView): PartialViewState =
            { previousViewState ->
                previousViewState.copy(searchView = searchView)
            }
    }
}

sealed class AddCityView {
    object Success : AddCityView()
    object Error : AddCityView()
    object Empty : AddCityView()
    object Loading : AddCityView()

    companion object {
        fun toAddCityView(result: Result<WeatherCity>): AddCityView =
            when (result) {
                is Result.Loading -> Loading
                is Result.Error -> Error
                is Result.Success -> Success
            }

        fun toPartialViewState(addCityView: AddCityView): PartialViewState =
            { previousViewState ->
                previousViewState.copy(addCityView = addCityView)
            }
    }
}

sealed class SearchViewEvent {
    object LoadDataEvent : SearchViewEvent()
    data class SearchDataEvent(val text: String) : SearchViewEvent()
    data class AddCityDataEvent(val text: String) : SearchViewEvent()
}