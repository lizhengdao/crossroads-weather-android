package com.umbrellait.weather.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.umbrellait.weather.R
import com.umbrellait.weather.databinding.MainFragmentBinding
import com.umbrellait.weather.ui.LinearItemDividers
import com.umbrellait.weather.utils.dpToPx
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel

@FlowPreview
@ExperimentalCoroutinesApi
class MainFragment : Fragment() {

    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!

    private val mainViewModel: MainViewModel by viewModel()

    private val mainAdapter: MainAdapter = MainAdapter(
        {
            findNavController().navigate(
                MainFragmentDirections.actionMainFragmentToDetailedFragment(it.name)
            )
        },
        {
            findNavController().navigate(R.id.action_mainFragment_to_searchFragment)
        }
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = MainFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainViewModel.state
            .onEach { handlerState(it) }
            .catch {  }
            .launchIn(lifecycleScope)

        binding.swipeRefreshLayout.setOnRefreshListener {
            lifecycleScope.launchWhenStarted { mainViewModel.sendMainViewEvent(MainViewEvent.LoadDataEvent) }
        }

        binding.cityWeatherListRecyclerView.adapter = mainAdapter
        binding.cityWeatherListRecyclerView.addItemDecoration(
            LinearItemDividers(
                12.dpToPx,
                24.dpToPx,
                ContextCompat.getColor(requireContext(), R.color.colorWhite)
            )
        )

        lifecycleScope.launchWhenStarted { mainViewModel.sendMainViewEvent(MainViewEvent.LoadDataEvent) }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun handlerState(mainViewState: MainViewState) {
        when (mainViewState.mainView) {
            is MainView.Loading -> {
                binding.swipeRefreshLayout.isRefreshing = true
            }
            is MainView.Error -> {
                binding.swipeRefreshLayout.isRefreshing = false
                mainAdapter.items = mainViewState.mainView.list
                mainAdapter.notifyDataSetChanged()
            }
            is MainView.Empty -> {
                binding.swipeRefreshLayout.isRefreshing = false
            }
            is MainView.Success -> {
                binding.swipeRefreshLayout.isRefreshing = false
                mainAdapter.items = mainViewState.mainView.list
                mainAdapter.notifyDataSetChanged()
            }
        }
    }
}