package com.umbrellait.weather.ui.several_days

import com.umbrellait.weather.domain.Daily
import com.umbrellait.weather.domain.Result

private typealias PartialViewState = (SeveralDaysViewState) -> SeveralDaysViewState

data class SeveralDaysViewState(
    val severalDaysView: SeveralDaysView = SeveralDaysView.Loading
)

sealed class SeveralDaysView {
    data class Success(val dailyList: List<Daily>): SeveralDaysView()
    object Error: SeveralDaysView()
    object Loading: SeveralDaysView()

    companion object {
        fun toSeveralDaysView(result: Result<List<Daily>>): SeveralDaysView =
            when(result) {
                is Result.Loading -> Loading
                is Result.Error -> Error
                is Result.Success -> Success(result.data)
            }

        fun toPartialViewState(severalDaysView: SeveralDaysView): PartialViewState =
            { previousViewState ->
                previousViewState.copy(severalDaysView = severalDaysView)
            }
    }
}

sealed class SeveralDaysViewEvent {
    object LoadDataEvent: SeveralDaysViewEvent()
}