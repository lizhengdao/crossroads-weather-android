package com.umbrellait.weather.ui.detailed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.umbrellait.weather.R
import com.umbrellait.weather.databinding.DetailedFragmentBinding
import com.umbrellait.weather.ui.LinearItemDividers
import com.umbrellait.weather.utils.dpToPx
import com.umbrellait.weather.utils.setThrottledClickListener
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

@FlowPreview
@ExperimentalCoroutinesApi
class DetailedFragment : Fragment() {

    private var _binding: DetailedFragmentBinding? = null
    private val binding get() = _binding!!

    private val detailedViewModel: DetailedViewModel by viewModel {
        parametersOf(arguments?.let { DetailedFragmentArgs.fromBundle(it).nameCity } ?: "")
    }

    private val detailedAdapter: DetailedAdapter = DetailedAdapter()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DetailedFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        detailedViewModel.state
            .onEach { handlerState(it) }
            .launchIn(lifecycleScope)

        binding.swipeRefreshLayout.setOnRefreshListener {
            lifecycleScope.launchWhenStarted { detailedViewModel.sendDetailedViewEvent(DetailedViewEvent.LoadDataEvent) }
        }

        binding.backArrow.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.cityWeatherDetailedRecyclerView.adapter = detailedAdapter
        binding.cityWeatherDetailedRecyclerView.addItemDecoration(
            LinearItemDividers(
                0.dpToPx,
                12.dpToPx,
                ContextCompat.getColor(requireContext(), R.color.colorWhite)
            )
        )

        binding.titleTextView.text = arguments?.let { DetailedFragmentArgs.fromBundle(it).nameCity } ?: ""
        binding.backArrow.setThrottledClickListener { findNavController().navigateUp() }

        lifecycleScope.launchWhenStarted { detailedViewModel.sendDetailedViewEvent(DetailedViewEvent.LoadDataEvent) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun handlerState(detailedViewState: DetailedViewState) {
        when (detailedViewState.detailedView) {
            is DetailedView.Loading -> {
                binding.swipeRefreshLayout.isRefreshing = true
            }
            is DetailedView.Error -> {
                binding.swipeRefreshLayout.isRefreshing = false
            }
            is DetailedView.Success -> {
                binding.severalDaysTextView.setOnClickListener {
                    val directions = DetailedFragmentDirections
                        .actionDetailedFragmentToSeveralDaysFragment(
                            detailedViewState.detailedView.cardState.weatherCity?.coord?.lat ?: "",
                            detailedViewState.detailedView.cardState.weatherCity?.coord?.lon ?: "",
                            detailedViewState.detailedView.cardState.weatherCity?.name ?: "")
                    findNavController().navigate(directions)
                }
                binding.swipeRefreshLayout.isRefreshing = false
                detailedAdapter.items = listOf(detailedViewState.detailedView.cardState)
                detailedAdapter.notifyDataSetChanged()
            }
        }
    }
}