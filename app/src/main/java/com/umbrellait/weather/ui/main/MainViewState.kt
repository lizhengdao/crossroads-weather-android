package com.umbrellait.weather.ui.main

import com.umbrellait.weather.R
import com.umbrellait.weather.domain.Result
import com.umbrellait.weather.domain.WeatherCity
import com.umbrellait.weather.domain.items.ButtonState
import com.umbrellait.weather.domain.items.DisplayableItem
import com.umbrellait.weather.domain.items.ErrorState

private typealias PartialViewState = (MainViewState) -> MainViewState

data class MainViewState(
    val mainView: MainView = MainView.Loading
)

sealed class MainView {
    data class Success(val list: List<DisplayableItem>): MainView()
    data class Error(val list: List<DisplayableItem>): MainView()
    object Empty: MainView()
    object Loading: MainView()

    companion object {
        fun toMainView(result: Result<List<WeatherCity>>): MainView =
            when(result) {
                is Result.Loading -> Loading
                is Result.Error -> Error(listOf(ErrorState))
                is Result.Success -> if (result.data.isNotEmpty()) Success(result.data + ButtonState(text = "Добавить", icon = R.drawable.ic_add)) else Empty
            }

        fun toPartialViewState(mainView: MainView): PartialViewState =
            { previousViewState ->
                previousViewState.copy(mainView = mainView)
            }
    }
}

sealed class MainViewEvent {
    object LoadDataEvent: MainViewEvent()
}