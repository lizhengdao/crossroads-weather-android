package com.umbrellait.weather.ui.detailed

import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.umbrellait.weather.domain.items.DisplayableItem
import com.umbrellait.weather.ui.delegate.*

class DetailedAdapter : ListDelegationAdapter<List<DisplayableItem>>(
    cardAdapterDelegate()
)