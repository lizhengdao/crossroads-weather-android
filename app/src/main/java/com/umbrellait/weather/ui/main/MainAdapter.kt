package com.umbrellait.weather.ui.main

import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.umbrellait.weather.domain.items.DisplayableItem
import com.umbrellait.weather.domain.WeatherCity
import com.umbrellait.weather.ui.delegate.buttonAdapterDelegate
import com.umbrellait.weather.ui.delegate.errorAdapterDelegate
import com.umbrellait.weather.ui.delegate.weatherCityLineAdapterDelegate

class MainAdapter(
    weatherCityItemClickedListener: (WeatherCity) -> Unit,
    addButtonClickedListener: () -> Unit
) : ListDelegationAdapter<List<DisplayableItem>>(
    weatherCityLineAdapterDelegate(weatherCityItemClickedListener),
    buttonAdapterDelegate(addButtonClickedListener),
    errorAdapterDelegate()
)