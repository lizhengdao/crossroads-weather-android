package com.umbrellait.weather.domain.items

import androidx.annotation.DrawableRes

data class ButtonState(
    val text: String,
    @DrawableRes
    val icon: Int?
): DisplayableItem