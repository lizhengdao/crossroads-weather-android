package com.umbrellait.weather.domain.items

import com.umbrellait.weather.domain.Daily
import com.umbrellait.weather.domain.WeatherCity
import com.umbrellait.weather.utils.CardSource

data class CardState (
    val weatherCity: WeatherCity?,
    val daily: Daily?,
    val cardSource: CardSource
): DisplayableItem