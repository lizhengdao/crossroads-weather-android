package com.umbrellait.weather.domain

data class Daily(
    val lat: String,
    val lon: String,
    val clouds: Int,
    val dt: Long,
    val feelsLike: FeelsLike,
    val humidity: Int,
    val pressure: Int,
    val sunrise: Long,
    val sunset: Long,
    val temp: Temp,
    val uvi: Double,
    val weather: Weather,
    val windDeg: Int,
    val windSpeed: Double
)

data class FeelsLike(
    val morn: Int,
    val day: Int,
    val eve: Int,
    val night: Int
)

data class Temp(
    val morn: Int,
    val day: Int,
    val eve: Int,
    val night: Int
)