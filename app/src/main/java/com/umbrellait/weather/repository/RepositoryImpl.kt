package com.umbrellait.weather.repository

import com.umbrellait.weather.database.Database
import com.umbrellait.weather.domain.Daily
import com.umbrellait.weather.domain.Result
import com.umbrellait.weather.domain.WeatherCity
import com.umbrellait.weather.network.Network
import com.umbrellait.weather.network.NetworkResult
import kotlinx.coroutines.flow.flowOf
import java.util.*

class RepositoryImpl(
    private val network: Network,
    private val database: Database
): Repository {

    override suspend fun getWeatherCity(nameCity: String, isSaveDatabase: Boolean): Result<WeatherCity> {
        val weatherCityList = database.getWeatherCityList()
        val delta = weatherCityList.firstOrNull { it.name == nameCity }?.let {
            (GregorianCalendar.getInstance().timeInMillis / 1000 - it.dt)
        } ?: DELTA_HOUR
        return if (delta >= DELTA_HOUR) {
            when(val result = network.getWeatherCity(nameCity)) {
                is NetworkResult.Success -> {
                    if (isSaveDatabase) {
                        database.insertOrUpdate(result.data)
                    }
                    Result.Success(result.data)
                }
                is NetworkResult.Error -> {
                        Result.Error(result.message, code = result.code)
                }
            }
        } else {
            Result.Success(weatherCityList.first { it.name == nameCity })
        }
    }

    override suspend fun getWeatherCityList(): Result<List<WeatherCity>> {
        var weatherCityList = database.getWeatherCityList()

        if (weatherCityList.isEmpty()) //return Result.Success(emptyList())
        {
            getWeatherCity("Минск")
            getWeatherCity("Москва")
            weatherCityList = database.getWeatherCityList()
        }


        val delta = weatherCityList.firstOrNull()?.let {
            (GregorianCalendar.getInstance().timeInMillis / 1000 - it.dt)
        } ?: DELTA_HOUR

        return if (delta >= DELTA_HOUR) {
            val idList = weatherCityList.map { it.id }.joinToString(separator = ",")
            when(val result = network.getWeatherCityList(idList)) {
                is NetworkResult.Success -> {
                    result.data.forEach { database.insertOrUpdate(it) }
                    Result.Success(result.data)
                }
                is NetworkResult.Error -> Result.Error(result.message)
            }
        } else {
            Result.Success(weatherCityList)
        }
    }

    override suspend fun getDailyList(lat: String, lon: String): Result<List<Daily>> {
        val dailyList = database.getDailyList(lat = lat, lon = lon)

        val delta = dailyList.firstOrNull()?.let {
            (GregorianCalendar.getInstance().timeInMillis / 1000 - it.dt)
        } ?: DELTA_DAY

        return if (delta >= DELTA_DAY) {
            when(val result = network.getWeatherCityInSevenDays(lat = lat, lon = lon)) {
                is NetworkResult.Success -> {
                    database.updateDailyList(dailyListOld = dailyList, dailyListNew = result.data)
                    Result.Success(result.data)
                }
                is NetworkResult.Error -> Result.Error(result.message)
            }
        } else {
            Result.Success(dailyList)
        }
    }

    companion object {
        private  const val DELTA_HOUR: Long = 60 * 60
        private  const val DELTA_DAY: Long = (60 * 60 * 24)
    }
}